package com.devcamp.orderapi.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.orderapi.models.Order;
import com.devcamp.orderapi.repository.OrderRepository;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> createOrders() {
        List<Order> orders = Arrays.asList(
                new Order(1, "ORD001", 1001, 2001, "Large", "Pepperoni", "VOUCHER001", 1500, 1200),
                new Order(2, "ORD002", 1002, 2002, "Medium", "Vegetarian", "VOUCHER002", 1200, 1000),
                new Order(3, "ORD003", 1003, 2003, "Small", "Margherita", "VOUCHER003", 1000, 900),
                new Order(4, "ORD004", 1004, 2004, "Medium", "Hawaiian", "VOUCHER004", 1300, 1100),
                new Order(5, "ORD005", 1005, 2005, "Small", "Pepperoni", "VOUCHER005", 950, 800),
                new Order(6, "ORD006", 1001, 2002, "Large", "Cheese", "VOUCHER006", 1400, 1150),
                new Order(7, "ORD007", 1002, 2003, "Small", "Veggie Deluxe", "VOUCHER007", 1050, 900),
                new Order(8, "ORD008", 1003, 2004, "Medium", "Mushroom", "VOUCHER008", 1200, 1000),
                new Order(9, "ORD009", 1004, 2005, "Large", "BBQ Chicken", "VOUCHER009", 1600, 1300),
                new Order(10, "ORD010", 1005, 2001, "Medium", "Supreme", "VOUCHER010", 1350, 1150));
        return orderRepository.saveAll(orders);

    }

    public List<Order> getOrders(String page, String size) {
        int pageNumber = Integer.parseInt(page);

        int pageSize = Integer.parseInt(size);
        Pageable pageableVoucher = PageRequest.of(pageNumber, pageSize);
        Page<Order> orderPage = orderRepository.findAll(pageableVoucher);
        List<Order> orderList = orderPage.getContent();
        return orderList;

    }

}
