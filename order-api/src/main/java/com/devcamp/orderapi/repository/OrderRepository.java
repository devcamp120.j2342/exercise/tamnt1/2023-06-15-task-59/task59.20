package com.devcamp.orderapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderapi.models.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

}
